package model;

import java.util.ArrayList;

public class Library {
	private String name;
	private int count;
	private ArrayList<Book> allBook = new ArrayList<Book>();
	private ArrayList<ReferenceBook> allRefBook = new ArrayList<ReferenceBook>();
	private Student stu;
	
	public void addBook(Book book) {
        allBook.add(book);
    }

	public void addRefBook(ReferenceBook refbook) {
		allRefBook.add(refbook);
	}

	public int bookCount() {
		// TODO Auto-generated method stub
		count += count;
		return count;
	}

	public void borrowBook(Student stu, String nameBook) {
		// TODO Auto-generated method stub
		int found = 0;
		for (Book book : allBook) {
			if (book.getNameBook().equals(nameBook)) {
				if (found == 0) {
					found = 1;
				}
				if (!book.isBorrowed()) {
					book.borrowed();
					found = 2;
					break;
				};
			}
		}
		if (found == 0) {
			System.out.println("Sorry, this book is not in our catalog.");
		} else if (found == 1) {
			System.out.println("Sorry, this book is already borrowed.");
		} else if (found == 2) {
			System.out.println(nameBook);
		}
		
	}

	public String getBorrowers() {
		// TODO Auto-generated method stub
		return stu.getName();
	}

	public void returnBook(String nameBook) {
		// TODO Auto-generated method stub
		boolean found = false;
		for (Book book : allBook) {
			if (book.getNameBook().equals(nameBook) && book.isBorrowed()) {
				book.returned();
				found = true;
				break;
			}
		}
		if (found) {
			System.out.println(nameBook);
		}
	}
}
