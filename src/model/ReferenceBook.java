package model;

public class ReferenceBook {
	private String nameBook;
	private int price;
	private String author;

	public ReferenceBook(String nameBook, int price, String author) {
		// TODO Auto-generated constructor stub
		this.nameBook = nameBook;
		this.price = price;
		this.author = author;
	}

	public String getNameBook(){
		return nameBook;
	}

	public int getPrice(){
		return price;
	}

	public String getAuthor(){
		return author;
	}

	public String getBook() {
		// TODO Auto-generated method stub
		return this.nameBook + " " + this.price + " " + this.author;
	}
}