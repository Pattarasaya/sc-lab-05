package view;

import model.Book;
import model.Library;
import model.ReferenceBook;
import model.Staff;
import model.Student;
import model.Teacher;

public class Main {
	public static void main(String[] args) {
		Library lib = new Library();
		
		Student stu = new Student("Vail", "Tang", "5510450410");
		Teacher teacher = new Teacher("A.Umaporn", "Pan", "D14");
		Staff staff = new Staff("Vail", "Tang");
		
		Book book = new Book("Big Java", 400, "Watson");
		Book book1 = new Book("A Game of Thrones", 500, "George R.R. Martin");
		ReferenceBook refbook = new ReferenceBook("ABC", 200, "Usa");
		
		System.out.println(book.getBook());
		System.out.println(refbook.getBook());
		
		lib.addBook(book);
		lib.addBook(book1);
		lib.addRefBook(refbook);


		System.out.println(lib.bookCount()); //3
		lib.borrowBook(stu, "Big Java");
		lib.borrowBook(stu, "A Game of Thrones");
		lib.borrowBook(stu, "Dee land");
		
//		System.out.println(lib.bookCount()); //2
//		System.out.println(book.getNameBook());
		System.out.println(stu.getBorrowBook()); //Big Java
//		System.out.println(lib.getBorrowers()); //Vail Tang
		System.out.println(stu.getName()); //Vail Tang
//		System.out.println(stu.getBorrowBook()); //empty string
	}
}
